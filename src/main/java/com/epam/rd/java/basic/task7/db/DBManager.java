package com.epam.rd.java.basic.task7.db;

import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;
	private String URL;

	private Connection getConnection(boolean autocommit) throws SQLException {
		Connection con = DriverManager.getConnection(URL);
		con.setAutoCommit(autocommit);
		con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
		return con;
	}

	private Connection getConnection() throws SQLException {
		return getConnection(true);
	}

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
		Properties p = new Properties ();
		try	(FileReader fileReader = new FileReader("app.properties")) {
			p.load(fileReader);
			URL = p.getProperty("connection.url");
		}  catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		try (Connection con = getConnection();
			 Statement st = con.createStatement();
			 ResultSet rs = st.executeQuery("SELECT * FROM users")) {

			 while (rs.next()) {
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setLogin(rs.getString("login"));
				users.add(user);
			 }
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Can't get all users", e);
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		Connection con = null;
		PreparedStatement preparedStatement = null;
		try {
			con = getConnection();
			preparedStatement = con.prepareStatement("INSERT INTO users VALUES (DEFAULT, ?)", Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, user.getLogin());
			preparedStatement.executeUpdate();
			ResultSet keys = preparedStatement.getGeneratedKeys();
			if (keys.next()) {
				user.setId(keys.getInt(1));
			}
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Can't insert user", e);
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		try (Connection con = getConnection();
			 PreparedStatement st = con.prepareStatement("DELETE FROM users WHERE id = ?")){

			for (User user : users) {
				int k = 0;
				st.setInt(++k, user.getId());
				st.executeUpdate();
			}
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Can't delete users", e);
		}
	}

	public User getUser(String login) throws DBException {
		try (Connection con = getConnection();
			PreparedStatement st = con.prepareStatement("SELECT * FROM users WHERE login=?")) {
			st.setString(1, login);
			User user = new User();
			try(ResultSet rs = st.executeQuery()) {
				if (rs.next()) {
					user.setId(rs.getInt("id"));
					user.setLogin(rs.getString("login"));
				}
			}
			return user;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Can not get users", e);
		}

	}

	public Team getTeam(String name) throws DBException {
		try (Connection con = getConnection();
			 PreparedStatement st = con.prepareStatement("SELECT * FROM teams WHERE name=?")) {
			st.setString(1, name);
			Team team = new Team();
			try(ResultSet rs = st.executeQuery()) {
				if (rs.next()) {
					team.setId(rs.getInt("id"));
					team.setName(rs.getString("name"));
				}
			}
			return team;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Can not get teams", e);
		}
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		try (Connection con = getConnection();
			 Statement st = con.createStatement();
			 ResultSet rs = st.executeQuery("SELECT * FROM teams")) {

			while (rs.next()) {
				Team team = new Team();
				team.setId(rs.getInt("id"));
				team.setName(rs.getString("name"));
				teams.add(team);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Can't find all teams", e);
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		Connection con = null;
		PreparedStatement preparedStatement = null;
		try {
			con = getConnection();
			preparedStatement = con.prepareStatement("INSERT INTO teams VALUES (DEFAULT, ?)", Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, team.getName());
			preparedStatement.executeUpdate();
			ResultSet keys = preparedStatement.getGeneratedKeys();
			if (keys.next()) {
				team.setId(keys.getInt(1));
			}
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Can't insert team", e);
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection con = null;
		PreparedStatement preparedStatement = null;
		try {
			con = getConnection(false);
			preparedStatement = con.prepareStatement("INSERT INTO users_teams VALUES (?, ?)");

			for (Team team: teams) {
				int k = 0;
				preparedStatement.setInt(++k, user.getId());
				preparedStatement.setInt(++k, team.getId());
				preparedStatement.executeUpdate();
			}
			con.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException ex) {
				throw new DBException("Can't rollback", ex);
			}
			throw new DBException("Can't set Teams for User", e);
		}


		return false;	
	}

	public List<Team> getUserTeams(User user) throws DBException {
		Connection con = null;
		PreparedStatement preparedStatement = null;
		List<Team> teams = new ArrayList<>();
		try {
			con = getConnection();
			preparedStatement = con.prepareStatement("select name FROM teams WHERE id in (select team_id from users_teams where user_id=?)");
			preparedStatement.setInt(1, user.getId());
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				Team team = new Team();
				team.setName(rs.getString("name"));
				teams.add(team);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Can't get User Teams", e);
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		try (Connection con = getConnection();
			 PreparedStatement st = con.prepareStatement("DELETE FROM teams WHERE id = ?")){
			st.setInt(1, team.getId());
			st.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Can't delete team", e);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		try (Connection con = getConnection();
			 PreparedStatement st = con.prepareStatement("UPDATE teams SET name = ? WHERE id = ?")){
			st.setString(1, team.getName());
			st.setInt(2, team.getId());
			st.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Can't update team", e);
		}
	}

}
